# Money Transfer Rest API

### About

Money Transfer RESTful API with Spring Boot. It uses an in-memory database H2 to store the data.
*Note*: It was decided to display all the amounts and balances in cents to perform accurate calculations involving money. Eventually to display the amount to the client we just need to convert that value.

### Prerequisites
- JDK 1.8+
- Maven 3+

### Stack
- Spring Boot
- Spring Data JPA
- Spring Data REST
- H2 in-memory DB

## Run

Build and run the app using maven

```bash
mvn package
java -jar target/money-transfer-0.0.1-SNAPSHOT.jar
```

Or run the app without packaging

```bash
mvn spring-boot:run
```

The app will start running at <http://localhost:8080>.

## Rest APIs

The app can be tested using a rest client like postman.
https://www.getpostman.com/collections/528a4b3153e9a99c2b15

### Account

#### Create Account resource

```
POST /accounts/
Accept: application/json
Content-Type: application/json

{
    "name" : "My First Account",
    "balance": 10000
}


RESPONSE: HTTP 201 (Created)
RESPONSE BODY:
{
    "id": 1,
    "name": "My First Account",
    "balance": 10000
}
Location header: http://localhost:8080/accounts/
```

#### Get all Accounts resource

```
GET /accounts/

RESPONSE: HTTP 200 (Ok)
RESPONSE BODY:
[
    {
        "id": 1,
        "name": "My First Account",
        "balance": 10000
    },
    {
        "id": 2,
        "name": "Client Account",
        "balance": 1000000000000000
    },
]
...
Location header: http://localhost:8080/accounts/
```

#### Get Account by Id resource

```
GET /accounts/{accountId}

RESPONSE: HTTP 200 (Ok)
RESPONSE BODY:
{
    "id": 1,
    "name": "My First Account",
    "balance": 10000
}
Location header: http://localhost:8080/accounts/{accountId}
```

### Transactions

#### Create a Transactions resource

```
POST /transactions/
Accept: application/json
Content-Type: application/json

{
    "senderId" : 1,
    "recipientId" : 2,
    "amount": 1000
}

RESPONSE: HTTP 201 (Created)
RESPONSE BODY:
{
    "id": 1,
    "senderId": 1,
    "recipientId": 2,
    "amount": 1000,
    "timestamp": "2018-12-29T10:32:07.942288Z",
    "valid": true
}

Location header: http://localhost:8080/transactions
```

#### Revert a Transactions resource

```
PATCH /transactions/{transactionId}?isRevert={true/false}
Accept: application/json
Content-Type: application/json

RESPONSE: HTTP 200 (OK)

Location header: http://localhost:8080/transactions/{transactionId}?isRevert={true/false}
```

#### Get all Transactions by Account Id resource

```
GET /transactions/account/{accountId}

RESPONSE: HTTP 200 (Ok)
RESPONSE BODY:
[
    {
        "id": 1,
        "senderId": 1,
        "recipientId": 2,
        "amount": 1000,
        "timestamp": "2018-12-29T10:32:07.942288Z",
        "valid": true
    }
    ...
]
Location header: http://localhost:8080/transactions/account/{accountId}
```

#### Get Transactions by Id resource

```
GET /transactions/{transactionId}

RESPONSE: HTTP 200 (Ok)
RESPONSE BODY:

{
    "id": 3,
    "senderId": 6,
    "recipientId": 10,
    "amount": 1000,
    "timestamp": "2018-12-29T10:32:07.942288Z",
    "valid": true
}

Location header: http://localhost:8080/transactions/{transactionId}
```
