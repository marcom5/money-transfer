package com.revolut.moneytransfer.datatransferobject;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.math.BigInteger;
import java.time.Instant;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TransactionResponseDTO {

    private Long id;

    private Long senderId;

    private Long recipientId;

    private BigInteger amount;

    private Instant timestamp;

    private boolean isValid;

    public TransactionResponseDTO(Long id,
                                  Long senderId,
                                  Long recipientId,
                                  BigInteger amount,
                                  Instant timestamp,
                                  boolean isValid) {
        this.id = id;
        this.senderId = senderId;
        this.recipientId = recipientId;
        this.amount = amount;
        this.timestamp = timestamp;
        this.isValid = isValid;
    }

    public Long getId() {
        return id;
    }

    public Long getSenderId() {
        return senderId;
    }

    public Long getRecipientId() {
        return recipientId;
    }

    public BigInteger getAmount() {
        return amount;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public boolean isValid() {
        return isValid;
    }

    public static TransactionResponseDTOBuilder newBuilder() {
        return new TransactionResponseDTOBuilder();
    }

    public static class TransactionResponseDTOBuilder {
        private Long id;
        private Long senderId;
        private Long recipientId;
        private BigInteger amount;
        private Instant timestamp;
        private boolean isValid;

        public TransactionResponseDTOBuilder setId(Long id) {
            this.id = id;
            return this;
        }

        public TransactionResponseDTOBuilder setSenderId(Long senderId) {
            this.senderId = senderId;
            return this;
        }

        public TransactionResponseDTOBuilder setRecipientId(Long recipientId) {
            this.recipientId = recipientId;
            return this;
        }

        public TransactionResponseDTOBuilder setAmount(BigInteger amount) {
            this.amount = amount;
            return this;
        }

        public TransactionResponseDTOBuilder setTimestamp(Instant timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public TransactionResponseDTOBuilder setValid(boolean isValid) {
            this.isValid = isValid;
            return this;
        }

        public TransactionResponseDTO createTransactionResponseDTO() {
            return new TransactionResponseDTO(id, senderId, recipientId, amount, timestamp, isValid);
        }

    }

    @Override
    public String toString() {
        return "{" +
                "\"id\":" + id +
                ",\"senderId\":" + senderId +
                ",\"recipientId\":" + recipientId +
                ",\"amount\":" + amount +
                ",\"timestamp\":\"" + timestamp + "\"" +
                ",\"valid\":" + isValid +
                '}';
    }

}
