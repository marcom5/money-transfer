package com.revolut.moneytransfer.datatransferobject;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.constraints.NotNull;
import java.math.BigInteger;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TransactionDTO {

    @NotNull(message = "The sender Id can not be null!")
    private Long senderId;

    @NotNull(message = "The recipient Id can not be null!")
    private Long recipientId;

    @NotNull(message = "The amount can not be null!")
    private BigInteger amount;

    public TransactionDTO(Long senderId,
                          Long recipientId,
                          BigInteger amount) {
        this.senderId = senderId;
        this.recipientId = recipientId;
        this.amount = amount;
    }

    public Long getSenderId() {
        return senderId;
    }

    public Long getRecipientId() {
        return recipientId;
    }

    public BigInteger getAmount() {
        return amount;
    }

    public static TransactionDTOBuilder newBuilder() {
        return new TransactionDTOBuilder();
    }

    public static class TransactionDTOBuilder {
        private Long senderId;
        private Long recipientId;
        private BigInteger amount;

        public TransactionDTOBuilder setSenderId(Long senderId) {
            this.senderId = senderId;
            return this;
        }

        public TransactionDTOBuilder setRecipientId(Long recipientId) {
            this.recipientId = recipientId;
            return this;
        }

        public TransactionDTOBuilder setAmount(BigInteger amount) {
            this.amount = amount;
            return this;
        }

        public TransactionDTO createTransactionDTO() {
            return new TransactionDTO(senderId, recipientId, amount);
        }

    }
}
