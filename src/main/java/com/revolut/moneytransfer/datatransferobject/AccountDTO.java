package com.revolut.moneytransfer.datatransferobject;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.constraints.NotNull;
import java.math.BigInteger;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountDTO {

    private Long id;

    @NotNull(message = "The name can not be null!")
    private String name;

    @NotNull(message = "The balance can not be null!")
    private BigInteger balance;

    public AccountDTO(Long id, String name, BigInteger balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public BigInteger getBalance() {
        return balance;
    }

    public static AccountDTOBuilder newBuilder() {
        return new AccountDTOBuilder();
    }

    public static class AccountDTOBuilder {
        private Long id;
        private String name;
        private BigInteger balance;

        public AccountDTOBuilder setId(Long id) {
            this.id = id;
            return this;
        }

        public AccountDTOBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public AccountDTOBuilder setBalance(BigInteger balance) {
            this.balance = balance;
            return this;
        }

        public AccountDTO createAccountDTO() {
            return new AccountDTO(id, name, balance);
        }

    }
}
