package com.revolut.moneytransfer.service.transaction;

import com.revolut.moneytransfer.datatransferobject.TransactionDTO;
import com.revolut.moneytransfer.domainobject.TransactionDO;
import com.revolut.moneytransfer.exception.ConstraintsViolationException;
import com.revolut.moneytransfer.exception.EntityNotFoundException;
import com.revolut.moneytransfer.exception.InvalidTransactionException;

import java.util.List;

public interface TransactionService {

    TransactionDO find(Long transactionId) throws EntityNotFoundException;

    List<TransactionDO> findAllByAccountId(Long accountId);

    TransactionDO create(TransactionDTO transactionDTO) throws ConstraintsViolationException, EntityNotFoundException, InvalidTransactionException;

    void revert(Long transactionId) throws ConstraintsViolationException, EntityNotFoundException, InvalidTransactionException;

}
