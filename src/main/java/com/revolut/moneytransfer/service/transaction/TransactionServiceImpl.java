package com.revolut.moneytransfer.service.transaction;

import com.revolut.moneytransfer.dataaccessobject.TransactionRepository;
import com.revolut.moneytransfer.datatransferobject.TransactionDTO;
import com.revolut.moneytransfer.domainobject.AccountDO;
import com.revolut.moneytransfer.domainobject.TransactionDO;
import com.revolut.moneytransfer.exception.ConstraintsViolationException;
import com.revolut.moneytransfer.exception.EntityNotFoundException;
import com.revolut.moneytransfer.exception.InvalidTransactionException;
import com.revolut.moneytransfer.service.account.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class TransactionServiceImpl implements TransactionService {

    private static Logger LOG = LoggerFactory.getLogger(TransactionService.class);

    private final TransactionRepository transactionRepository;

    private final AccountService accountService;

    private final Map<Long, ReentrantLock> locks;

    public TransactionServiceImpl(final TransactionRepository transactionRepository, final AccountService accountService) {
        this.transactionRepository = transactionRepository;
        this.accountService = accountService;
        this.locks = new HashMap<>();
    }

    @Override
    public TransactionDO find(Long transactionId) throws EntityNotFoundException {
        final Optional<TransactionDO> transactionDO = transactionRepository.findById(transactionId);
        return transactionDO.orElseThrow(() -> new EntityNotFoundException("Could not find entity with id: " + transactionId));
    }

    @Override
    public List<TransactionDO> findAllByAccountId(Long accountId) {
        return Stream.of(transactionRepository.findBySenderId(accountId), transactionRepository.findByRecipientId(accountId))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public TransactionDO create(TransactionDTO transactionDTO) throws ConstraintsViolationException,
            EntityNotFoundException, InvalidTransactionException {

        final TransactionDO transactionDO = createTransactionDO(transactionDTO);

        try {
            return transactionRepository.save(transactionDO);
        } catch (DataIntegrityViolationException e) {
            LOG.warn("Some constraints are thrown due to transaction creation", e);
            throw new ConstraintsViolationException(e.getMessage());
        }

    }

    private TransactionDO createTransactionDO(TransactionDTO transactionDTO) throws EntityNotFoundException, InvalidTransactionException {
        final AccountDO sender;
        final AccountDO recipient;

        final ReentrantLock senderLock = this.getLock(transactionDTO.getSenderId());
        senderLock.lock();
        try {
            final ReentrantLock recipientLock = this.getLock(transactionDTO.getRecipientId());
            recipientLock.lock();
            try {
                sender = accountService.find(transactionDTO.getSenderId());
                if (sender.getBalance().compareTo(transactionDTO.getAmount()) < 0) {
                    LOG.warn("Not enough funds to process the transaction!");
                    throw new InvalidTransactionException("Not enough funds to process the transaction!");
                }
                recipient = accountService.find(transactionDTO.getRecipientId());

                sender.setBalance(sender.getBalance().subtract(transactionDTO.getAmount()));
                recipient.setBalance(recipient.getBalance().add(transactionDTO.getAmount()));
            } finally {
                recipientLock.unlock();
            }
        } finally {
            senderLock.unlock();
        }
        return new TransactionDO(sender, recipient, transactionDTO.getAmount(), Instant.now(), true);
    }

    @Override
    @Transactional
    public void revert(Long transactionId) throws ConstraintsViolationException, EntityNotFoundException,
            InvalidTransactionException {
        final TransactionDO transaction = find(transactionId);

        if (!transaction.isValid()) {
            LOG.warn("Cannot revert an invalid transaction!");
            throw new InvalidTransactionException("Cannot revert an invalid transaction!");
        }
        create(new TransactionDTO(transaction.getRecipient().getId(), transaction.getSender().getId(), transaction.getAmount()));

        transaction.setValid(false);
    }

    private ReentrantLock getLock(Long id) {
        synchronized (locks) {
            final ReentrantLock lock;
            if (locks.containsKey(id)) {
                lock = locks.get(id);
            } else {
                lock = new ReentrantLock();
                locks.put(id, lock);
            }
            return lock;
        }
    }

}
