package com.revolut.moneytransfer.service.account;

import com.revolut.moneytransfer.domainobject.AccountDO;
import com.revolut.moneytransfer.exception.ConstraintsViolationException;
import com.revolut.moneytransfer.exception.EntityNotFoundException;

import java.util.List;

public interface AccountService {

    AccountDO find(Long accountId) throws EntityNotFoundException;

    AccountDO create(AccountDO accountDO) throws ConstraintsViolationException;

    List<AccountDO> findAll();

}
