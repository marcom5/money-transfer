package com.revolut.moneytransfer.service.account;

import com.revolut.moneytransfer.dataaccessobject.AccountRepository;
import com.revolut.moneytransfer.domainobject.AccountDO;
import com.revolut.moneytransfer.exception.ConstraintsViolationException;
import com.revolut.moneytransfer.exception.EntityNotFoundException;
import com.revolut.moneytransfer.service.transaction.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService {

    private static Logger LOG = LoggerFactory.getLogger(TransactionService.class);

    private final AccountRepository accountRepository;

    public AccountServiceImpl(final AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public AccountDO find(Long accountId) throws EntityNotFoundException {
        final Optional<AccountDO> accountDO = accountRepository.findById(accountId);
        return accountDO.orElseThrow(() -> new EntityNotFoundException("Could not find entity with id: " + accountId));
    }

    @Override
    public AccountDO create(AccountDO accountDO) throws ConstraintsViolationException {
        final AccountDO account;
        try {
            account = accountRepository.save(accountDO);
        } catch (DataIntegrityViolationException e) {
            LOG.warn("Some constraints are thrown due to account creation!", e);
            throw new ConstraintsViolationException(e.getMessage());
        }
        return account;
    }

    @Override
    public List<AccountDO> findAll() {
        return accountRepository.findAll();
    }

}
