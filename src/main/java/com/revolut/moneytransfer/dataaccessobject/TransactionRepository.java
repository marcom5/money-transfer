package com.revolut.moneytransfer.dataaccessobject;

import com.revolut.moneytransfer.domainobject.TransactionDO;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransactionRepository extends JpaRepository<TransactionDO, Long> {

    List<TransactionDO> findBySenderId(Long senderId);

    List<TransactionDO> findByRecipientId(Long recipientId);

}
