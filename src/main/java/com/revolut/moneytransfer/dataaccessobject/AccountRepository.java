package com.revolut.moneytransfer.dataaccessobject;

import com.revolut.moneytransfer.domainobject.AccountDO;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<AccountDO, Long> {
}
