package com.revolut.moneytransfer.domainobject;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigInteger;
import java.util.Set;

@Entity
@Table(
        name = "account",
        uniqueConstraints = @UniqueConstraint(name = "uc_name", columnNames = {"name"})
)
public class AccountDO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    @NotNull(message = "Name can not be null!")
    private String name;

    @Column(nullable = false)
    private BigInteger balance;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sender")
    private Set<TransactionDO> senders;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "recipient")
    private Set<TransactionDO> recipients;

    public AccountDO() {
    }

    public AccountDO(String name, BigInteger balance) {
        this.name = name;
        this.balance = balance;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigInteger getBalance() {
        return balance;
    }

    public void setBalance(BigInteger balance) {
        this.balance = balance;
    }

    public Set<TransactionDO> getSenders() {
        return senders;
    }

    public void setSenders(Set<TransactionDO> senders) {
        this.senders = senders;
    }

    public Set<TransactionDO> getRecipients() {
        return recipients;
    }

    public void setRecipients(Set<TransactionDO> recipients) {
        this.recipients = recipients;
    }

}
