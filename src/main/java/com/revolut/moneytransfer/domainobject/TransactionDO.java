package com.revolut.moneytransfer.domainobject;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigInteger;
import java.time.Instant;

@Entity
@Table(name = "transaction")
public class TransactionDO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "sender_id", nullable = false)
    private AccountDO sender;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "recipient_id", nullable = false)
    private AccountDO recipient;

    @Min(0)
    @Column(nullable = false)
    @NotNull(message = "Amount can not be null!")
    private BigInteger amount;

    @Column(nullable = false)
    @NotNull(message = "Timestamp can not be null!")
    private Instant timestamp;

    @Column(nullable = false)
    private boolean isValid = true;

    public TransactionDO() {
    }

    public TransactionDO(AccountDO sender,
                         AccountDO recipient,
                         BigInteger amount,
                         Instant timestamp,
                         boolean isValid) {
        this.sender = sender;
        this.recipient = recipient;
        this.amount = amount;
        this.timestamp = timestamp;
        this.isValid = isValid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AccountDO getSender() {
        return sender;
    }

    public void setSender(AccountDO sender) {
        this.sender = sender;
    }

    public AccountDO getRecipient() {
        return recipient;
    }

    public void setRecipient(AccountDO recipient) {
        this.recipient = recipient;
    }

    public BigInteger getAmount() {
        return amount;
    }

    public void setAmount(BigInteger amount) {
        this.amount = amount;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }

}
