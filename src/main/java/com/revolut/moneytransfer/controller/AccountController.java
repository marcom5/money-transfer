package com.revolut.moneytransfer.controller;

import com.revolut.moneytransfer.controller.mapper.AccountMapper;
import com.revolut.moneytransfer.datatransferobject.AccountDTO;
import com.revolut.moneytransfer.domainobject.AccountDO;
import com.revolut.moneytransfer.exception.ConstraintsViolationException;
import com.revolut.moneytransfer.exception.EntityNotFoundException;
import com.revolut.moneytransfer.service.account.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("accounts")
public class AccountController {

    private final AccountService accountService;

    @Autowired
    public AccountController(final AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/{accountId}")
    public AccountDTO getAccount(@Valid @PathVariable long accountId) throws EntityNotFoundException {
        return AccountMapper.makeAccountDTO(accountService.find(accountId));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AccountDTO createAccount(@Valid @RequestBody AccountDTO accountDTO) throws ConstraintsViolationException {
        final AccountDO accountDO = new AccountDO(accountDTO.getName(), accountDTO.getBalance());
        return AccountMapper.makeAccountDTO(accountService.create(accountDO));
    }

    @GetMapping
    public List<AccountDTO> getAccounts() {
        return AccountMapper.makeAccountDTOList(accountService.findAll());
    }

}
