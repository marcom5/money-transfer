package com.revolut.moneytransfer.controller;

import com.revolut.moneytransfer.controller.mapper.TransactionMapper;
import com.revolut.moneytransfer.datatransferobject.TransactionDTO;
import com.revolut.moneytransfer.datatransferobject.TransactionResponseDTO;
import com.revolut.moneytransfer.exception.ConstraintsViolationException;
import com.revolut.moneytransfer.exception.EntityNotFoundException;
import com.revolut.moneytransfer.exception.InvalidTransactionException;
import com.revolut.moneytransfer.service.transaction.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("transactions")
public class TransactionController {

    private final TransactionService transactionService;

    @Autowired
    public TransactionController(final TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @GetMapping("/{transactionId}")
    public TransactionResponseDTO getTransaction(@Valid @PathVariable long transactionId) throws EntityNotFoundException {
        return TransactionMapper.makeTransactionResponseDTO(transactionService.find(transactionId));
    }

    @GetMapping("/account/{accountId}")
    public List<TransactionResponseDTO> getTransactions(@Valid @PathVariable long accountId) {
        return TransactionMapper.makeTransactionResponseDTOList(transactionService.findAllByAccountId(accountId));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TransactionResponseDTO createTransaction(@Valid @RequestBody TransactionDTO transactionDTO)
            throws ConstraintsViolationException, EntityNotFoundException, InvalidTransactionException {

        return TransactionMapper.makeTransactionResponseDTO(transactionService.create(transactionDTO));
    }

    @PatchMapping("/{transactionId}")
    @ResponseStatus(HttpStatus.OK)
    public void revertTransaction(@Valid @PathVariable long transactionId, @Valid @RequestParam Boolean isRevert)
            throws ConstraintsViolationException, EntityNotFoundException, InvalidTransactionException {
        if (isRevert) {
            transactionService.revert(transactionId);
        } else {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }
    }

}
