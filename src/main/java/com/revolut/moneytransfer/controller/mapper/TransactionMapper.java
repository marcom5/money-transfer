package com.revolut.moneytransfer.controller.mapper;

import com.revolut.moneytransfer.datatransferobject.TransactionResponseDTO;
import com.revolut.moneytransfer.domainobject.TransactionDO;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class TransactionMapper {

    public static TransactionResponseDTO makeTransactionResponseDTO(TransactionDO transactionDO) {
        TransactionResponseDTO.TransactionResponseDTOBuilder builder = TransactionResponseDTO.newBuilder()
                .setId(transactionDO.getId())
                .setSenderId(transactionDO.getSender().getId())
                .setRecipientId(transactionDO.getRecipient().getId())
                .setAmount(transactionDO.getAmount())
                .setTimestamp(transactionDO.getTimestamp())
                .setValid(transactionDO.isValid());

        return builder.createTransactionResponseDTO();
    }

    public static List<TransactionResponseDTO> makeTransactionResponseDTOList(Collection<TransactionDO> transactions) {
        return transactions.stream()
                .map(TransactionMapper::makeTransactionResponseDTO)
                .collect(Collectors.toList());
    }

}
