package com.revolut.moneytransfer.controller.mapper;

import com.revolut.moneytransfer.datatransferobject.AccountDTO;
import com.revolut.moneytransfer.domainobject.AccountDO;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class AccountMapper {

    public static AccountDTO makeAccountDTO(AccountDO accountDO) {
        AccountDTO.AccountDTOBuilder accountDTOBuilder = AccountDTO.newBuilder()
                .setId(accountDO.getId())
                .setName(accountDO.getName())
                .setBalance(accountDO.getBalance());

        return accountDTOBuilder.createAccountDTO();
    }

    public static List<AccountDTO> makeAccountDTOList(Collection<AccountDO> accounts) {
        return accounts.stream()
                .map(AccountMapper::makeAccountDTO)
                .collect(Collectors.toList());
    }

}
