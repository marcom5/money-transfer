-- Script to init DB

-- Create 2 accounts
insert into account(id, name, balance) values (1, 'Bank', 100000000);
insert into account(id, name, balance) values (2, 'Client', 1000000000000000);

-- Create 1 transaction
insert into transaction(id, sender_id, recipient_id, amount, timestamp, is_valid) values (1, 1, 2, 10, '2018-12-29T02:26:34.056825Z', true);
