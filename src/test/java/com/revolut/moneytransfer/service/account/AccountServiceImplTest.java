package com.revolut.moneytransfer.service.account;

import com.revolut.moneytransfer.dataaccessobject.AccountRepository;
import com.revolut.moneytransfer.domainobject.AccountDO;
import com.revolut.moneytransfer.exception.ConstraintsViolationException;
import com.revolut.moneytransfer.exception.EntityNotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigInteger;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
public class AccountServiceImplTest {

    private AccountService accountService;

    @MockBean
    private AccountRepository mockAccountRepository;

    @Before
    public void setUp() {
        this.accountService = new AccountServiceImpl(mockAccountRepository);
    }

    @Test
    public void shouldReturnAccountIfExist() throws Exception {
        final AccountDO accountDO = new AccountDO("Test account", new BigInteger("1000000"));
        accountDO.setId(1L);
        given(mockAccountRepository.findById(1L)).willReturn(Optional.of(accountDO));
        final AccountDO result = accountService.find(1L);

        assertEquals(Long.valueOf(1L), result.getId());
        assertEquals(new BigInteger("1000000"), result.getBalance());
        assertEquals("Test account", result.getName());

    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionIfAccountDoesNotExist() throws Exception {
        given(mockAccountRepository.findById(Mockito.anyLong())).willReturn(Optional.empty());
        accountService.find(99L);
    }

    @Test
    public void shouldCreateAccountSuccessfully() throws Exception {
        final AccountDO accountDO = new AccountDO("Test account", new BigInteger("1000000"));
        final AccountDO resultAccountDO = new AccountDO("Test account", new BigInteger("1000000"));
        resultAccountDO.setId(1L);

        given(mockAccountRepository.save(accountDO)).willReturn(resultAccountDO);
        final AccountDO result = accountService.create(accountDO);

        assertEquals(Long.valueOf(1L), result.getId());
        assertEquals(new BigInteger("1000000"), result.getBalance());
        assertEquals("Test account", result.getName());
    }

    @Test(expected = ConstraintsViolationException.class)
    public void shouldThrowExceptionIfConstraintsAreViolated() throws ConstraintsViolationException {
        final AccountDO accountDO = new AccountDO("Test account", new BigInteger("1000000"));
        given(mockAccountRepository.save(Mockito.any(AccountDO.class)))
                .willThrow(new DataIntegrityViolationException("Alert!"));

        accountService.create(accountDO);
    }

    @Test
    public void shouldReturnAListOfAccountsIfAtLeastOneExist() {
        final AccountDO accountDO = new AccountDO("Test account", new BigInteger("1000000"));
        accountDO.setId(1L);

        final List<AccountDO> accounts = Collections.singletonList(accountDO);
        given(mockAccountRepository.findAll()).willReturn(accounts);

        final List<AccountDO> result = accountService.findAll();
        assertEquals(1, result.size());
    }

    @Test
    public void shouldReturnAnEmptyListIfNoAccountExist() {
        given(mockAccountRepository.findAll()).willReturn(Collections.emptyList());
        assertEquals(Collections.emptyList(), accountService.findAll());
    }

}