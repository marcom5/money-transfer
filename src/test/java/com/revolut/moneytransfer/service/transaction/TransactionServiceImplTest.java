package com.revolut.moneytransfer.service.transaction;

import com.revolut.moneytransfer.dataaccessobject.TransactionRepository;
import com.revolut.moneytransfer.datatransferobject.TransactionDTO;
import com.revolut.moneytransfer.domainobject.AccountDO;
import com.revolut.moneytransfer.domainobject.TransactionDO;
import com.revolut.moneytransfer.exception.ConstraintsViolationException;
import com.revolut.moneytransfer.exception.EntityNotFoundException;
import com.revolut.moneytransfer.exception.InvalidTransactionException;
import com.revolut.moneytransfer.service.account.AccountService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigInteger;
import java.time.Instant;
import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
public class TransactionServiceImplTest {

    private static final Instant INSTANT_NOW = Instant.now();

    @MockBean
    private AccountService mockAccountService;

    @MockBean
    private TransactionRepository mockTransactionsRepository;

    private TransactionService transactionService;

    private TransactionDTO transactionDTO;

    private AccountDO accountDO1;

    private AccountDO accountDO2;

    private AccountDO resultAccountDO1;

    private AccountDO resultAccountDO2;

    private TransactionDO resultTransactionDO;

    @Before
    public void setUp() {
        transactionDTO = new TransactionDTO(1L, 2L, new BigInteger("1000"));

        accountDO1 = new AccountDO("Test account 1", new BigInteger("1000"));
        accountDO1.setId(1L);
        accountDO2 = new AccountDO("Test account 2", new BigInteger("0"));
        accountDO1.setId(2L);

        resultAccountDO1 = new AccountDO("Test account 1", new BigInteger("0"));
        resultAccountDO1.setId(1L);
        resultAccountDO2 = new AccountDO("Test account 2", new BigInteger("1000"));
        resultAccountDO2.setId(2L);
        resultTransactionDO = new TransactionDO(resultAccountDO1, resultAccountDO2, new BigInteger("1000"), INSTANT_NOW, true);
        resultTransactionDO.setId(1L);

        this.transactionService = new TransactionServiceImpl(mockTransactionsRepository, mockAccountService);
    }

    @Test
    public void shouldReturnTransactionIfExist() throws Exception {
        final TransactionDO transactionDO = new TransactionDO(accountDO1, accountDO2, new BigInteger("10"), INSTANT_NOW, true);
        transactionDO.setId(1L);

        given(mockTransactionsRepository.findById(1L)).willReturn(Optional.of(transactionDO));
        final TransactionDO result = transactionService.find(1L);

        assertEquals(Long.valueOf(1L), result.getId());
        assertEquals(accountDO1, result.getSender());
        assertEquals(accountDO2, result.getRecipient());
        assertEquals(new BigInteger("10"), result.getAmount());
        assertEquals(INSTANT_NOW, result.getTimestamp());
        assertTrue(result.isValid());

    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionIfTransactionDoesNotExist() throws Exception {
        given(mockTransactionsRepository.findById(Mockito.anyLong())).willReturn(Optional.empty());
        this.transactionService.find(99L);
    }

    @Test
    public void shouldReturnAListOfTransactionsPerAccountsIfAtLeastOneExist() {
        final TransactionDO transactionDO = new TransactionDO(accountDO1, accountDO2, new BigInteger("10"), INSTANT_NOW, true);
        transactionDO.setId(1L);

        final List<TransactionDO> accounts = Collections.singletonList(transactionDO);
        given(mockTransactionsRepository.findByRecipientId(1L)).willReturn(accounts);

        final List<TransactionDO> result = transactionService.findAllByAccountId(1L);
        assertEquals(1, result.size());
        assertEquals(Long.valueOf(1L), result.get(0).getId());
        assertEquals(accountDO1, result.get(0).getSender());
        assertEquals(accountDO2, result.get(0).getRecipient());
        assertEquals(new BigInteger("10"), result.get(0).getAmount());
        assertEquals(INSTANT_NOW, result.get(0).getTimestamp());
        assertTrue(result.get(0).isValid());
    }

    @Test
    public void shouldReturnAnEmptyListIfNoTransactionForAccount() {
        given(mockTransactionsRepository.findByRecipientId(99L)).willReturn(Collections.emptyList());
        given(mockTransactionsRepository.findBySenderId(99L)).willReturn(Collections.emptyList());

        assertEquals(Collections.emptyList(), transactionService.findAllByAccountId(99L));
    }

    @Test
    public void shouldCreateTransactionSuccessfully() throws Exception {
        given(mockAccountService.find(transactionDTO.getSenderId())).willReturn(accountDO1);
        given(mockAccountService.find(transactionDTO.getRecipientId())).willReturn(accountDO2);
        given(mockTransactionsRepository.save(any(TransactionDO.class))).willReturn(resultTransactionDO);

        final TransactionDO result = transactionService.create(transactionDTO);

        assertEquals(Long.valueOf(1L), result.getId());
        assertEquals(INSTANT_NOW, result.getTimestamp());
        assertEquals(new BigInteger("1000"), result.getAmount());
        assertEquals(resultAccountDO1, result.getSender());
        assertEquals(resultAccountDO2, result.getRecipient());
    }

    @Test(expected = ConstraintsViolationException.class)
    public void shouldThrowExceptionIfConstraintsAreViolated() throws Exception {
        given(mockAccountService.find(transactionDTO.getSenderId())).willReturn(accountDO1);
        given(mockAccountService.find(transactionDTO.getRecipientId())).willReturn(accountDO2);
        given(mockTransactionsRepository.save(any(TransactionDO.class))).willReturn(resultTransactionDO);

        given(mockTransactionsRepository.save(Mockito.any(TransactionDO.class)))
                .willThrow(new DataIntegrityViolationException("Alert!"));

        transactionService.create(transactionDTO);
    }

    @Test(expected = InvalidTransactionException.class)
    public void shouldThrowExceptionIfInvalidTransaction() throws Exception {
        final AccountDO accountWithNoBalance = new AccountDO("Test account", new BigInteger("0"));
        accountWithNoBalance.setId(1L);
        given(mockAccountService.find(transactionDTO.getSenderId())).willReturn(accountWithNoBalance);

        transactionService.create(transactionDTO);
    }

    @Test
    public void shouldRevertTransactionSuccessfullyIfValid() throws Exception {
        final TransactionDO transactionDO = new TransactionDO(resultAccountDO1,resultAccountDO2,new BigInteger("1000"), INSTANT_NOW, true);
        transactionDO.setId(1L);

        final TransactionDO resultTransactionDO = new TransactionDO(resultAccountDO2, resultAccountDO1, new BigInteger("1000"), INSTANT_NOW, true);
        resultTransactionDO.setId(1L);
        given(mockTransactionsRepository.findById(1L)).willReturn(Optional.of(transactionDO));
        given(mockAccountService.find(transactionDO.getSender().getId())).willReturn(accountDO2);
        given(mockAccountService.find(transactionDO.getRecipient().getId())).willReturn(accountDO1);
        given(mockTransactionsRepository.save(any(TransactionDO.class))).willReturn(resultTransactionDO);

        transactionService.revert(1L);
    }

   @Test(expected = InvalidTransactionException.class)
    public void revertShouldThrowExceptionIfInvalidTransaction() throws Exception {
       final TransactionDO transactionDO = new TransactionDO(resultAccountDO2, resultAccountDO1, new BigInteger("1000"), INSTANT_NOW, false);
       transactionDO.setId(1L);
       given(mockTransactionsRepository.findById(1L)).willReturn(Optional.of(transactionDO));

       transactionService.revert(1L);
    }

}