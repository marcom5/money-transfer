package com.revolut.moneytransfer.integrationtests;

import com.revolut.moneytransfer.MoneyTransferApplication;
import com.revolut.moneytransfer.datatransferobject.TransactionDTO;
import com.revolut.moneytransfer.datatransferobject.TransactionResponseDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigInteger;
import java.time.Instant;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = MoneyTransferApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class TransactionControllerIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void shouldRetrieveTransactionByIdIfExists() {
        ResponseEntity<TransactionResponseDTO> transactionsResponse = restTemplate.getForEntity("/transactions/1", TransactionResponseDTO.class);

        assertThat(transactionsResponse.getHeaders().get("Content-type")).containsOnly("application/json;charset=UTF-8");
        assertEquals(HttpStatus.OK, transactionsResponse.getStatusCode());
        assertEquals(Long.valueOf(1L), transactionsResponse.getBody().getId());
        assertEquals(Long.valueOf(1L), transactionsResponse.getBody().getSenderId());
        assertEquals(Long.valueOf(2L), transactionsResponse.getBody().getRecipientId());
        assertEquals(new BigInteger("10"), transactionsResponse.getBody().getAmount());
        assertEquals(Instant.parse("2018-12-29T02:26:34.056825Z"), transactionsResponse.getBody().getTimestamp());
    }

    @Test
    public void shouldNotRetrieveTransactionByIdIfNotExists() {
        ResponseEntity<TransactionResponseDTO> transactionsResponse = restTemplate.getForEntity("/transactions/99/transaction", TransactionResponseDTO.class);

        assertThat(transactionsResponse.getHeaders().get("Content-type")).containsOnly("application/json;charset=UTF-8");
        assertEquals(HttpStatus.NOT_FOUND, transactionsResponse.getStatusCode());
        assertNull(transactionsResponse.getBody().getId());
        assertNull(transactionsResponse.getBody().getSenderId());
        assertNull(transactionsResponse.getBody().getRecipientId());
        assertNull(transactionsResponse.getBody().getAmount());
    }

    @Test
    public void shouldRetrieveTransactionsByAccountId() {
        ResponseEntity<List> transactionsResponse = restTemplate.getForEntity("/transactions/account/1", List.class);

        assertThat(transactionsResponse.getHeaders().get("Content-type")).containsOnly("application/json;charset=UTF-8");
        assertEquals(HttpStatus.OK, transactionsResponse.getStatusCode());
        assertThat(transactionsResponse.getBody()).isNotEmpty();
    }


    @Test
    public void shouldCreateANewTransaction() {
        TransactionDTO transactionDTO = TransactionDTO.newBuilder().setSenderId(1L)
                .setRecipientId(2L).setAmount(new BigInteger("10")).createTransactionDTO();
        ResponseEntity<TransactionResponseDTO> transactionsResponse = restTemplate.postForEntity("/transactions/", transactionDTO, TransactionResponseDTO.class);

        assertThat(transactionsResponse.getHeaders().get("Content-type")).containsOnly("application/json;charset=UTF-8");
        assertEquals(HttpStatus.CREATED, transactionsResponse.getStatusCode());
        assertEquals(Long.valueOf(2L), transactionsResponse.getBody().getId());
        assertEquals(Long.valueOf(1L), transactionsResponse.getBody().getSenderId());
        assertEquals(Long.valueOf(2L), transactionsResponse.getBody().getRecipientId());
        assertEquals(new BigInteger("10"), transactionsResponse.getBody().getAmount());
    }

    @Test
    public void shouldNotCreateANewTransactionIfReqFieldMissing() {
        TransactionDTO transactionDTO = TransactionDTO.newBuilder().createTransactionDTO();
        ResponseEntity<TransactionResponseDTO> transactionsResponse = restTemplate.postForEntity("/transactions/", transactionDTO, TransactionResponseDTO.class);

        assertThat(transactionsResponse.getHeaders().get("Content-type")).containsOnly("application/json;charset=UTF-8");
        assertEquals(HttpStatus.BAD_REQUEST, transactionsResponse.getStatusCode());
    }

}
