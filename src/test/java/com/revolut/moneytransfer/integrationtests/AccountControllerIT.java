package com.revolut.moneytransfer.integrationtests;

import com.revolut.moneytransfer.MoneyTransferApplication;
import com.revolut.moneytransfer.datatransferobject.AccountDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigInteger;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = MoneyTransferApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class AccountControllerIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void shouldRetrieveAccountByIdIfExists() {
        ResponseEntity<AccountDTO> accountResponse = restTemplate.getForEntity("/accounts/1", AccountDTO.class);

        assertThat(accountResponse.getHeaders().get("Content-type")).containsOnly("application/json;charset=UTF-8");
        assertEquals(HttpStatus.OK, accountResponse.getStatusCode());
        assertEquals(Long.valueOf(1L), accountResponse.getBody().getId());
        assertEquals("Bank", accountResponse.getBody().getName());
        assertEquals(new BigInteger("100000000"), accountResponse.getBody().getBalance());
    }

    @Test
    public void shouldNotRetrieveAccountByIdIfNotExists() {
        ResponseEntity<AccountDTO> accountResponse = restTemplate.getForEntity("/accounts/99", AccountDTO.class);

        assertThat(accountResponse.getHeaders().get("Content-type")).containsOnly("application/json;charset=UTF-8");
        assertEquals(HttpStatus.NOT_FOUND, accountResponse.getStatusCode());
        assertNull(accountResponse.getBody().getId());
        assertNull(accountResponse.getBody().getName());
        assertNull(accountResponse.getBody().getBalance());
    }


    @Test
    public void shouldCreateANewAccount() {
        AccountDTO accountDTO = AccountDTO.newBuilder().setName("Test").setBalance(new BigInteger("10")).createAccountDTO();
        ResponseEntity<AccountDTO> accountResponse = restTemplate.postForEntity("/accounts/", accountDTO, AccountDTO.class);

        assertThat(accountResponse.getHeaders().get("Content-type")).containsOnly("application/json;charset=UTF-8");
        assertEquals(HttpStatus.CREATED, accountResponse.getStatusCode());
        assertEquals(Long.valueOf(3L), accountResponse.getBody().getId());
        assertEquals("Test", accountResponse.getBody().getName());
        assertEquals(new BigInteger("10"), accountResponse.getBody().getBalance());
    }

    @Test
    public void shouldNotCreateANewAccountIfReqFieldMissing() {
        AccountDTO accountDTO = AccountDTO.newBuilder().createAccountDTO();
        ResponseEntity<AccountDTO> accountResponse = restTemplate.postForEntity("/accounts/", accountDTO, AccountDTO.class);

        assertThat(accountResponse.getHeaders().get("Content-type")).containsOnly("application/json;charset=UTF-8");
        assertEquals(HttpStatus.BAD_REQUEST, accountResponse.getStatusCode());
    }

    @Test
    public void shouldRetrieveAccounts() {
        ResponseEntity<List> accountResponse = restTemplate.getForEntity("/accounts", List.class);

        assertThat(accountResponse.getHeaders().get("Content-type")).containsOnly("application/json;charset=UTF-8");
        assertEquals(HttpStatus.OK, accountResponse.getStatusCode());
        assertThat(accountResponse.getBody()).isNotEmpty();
    }

}
