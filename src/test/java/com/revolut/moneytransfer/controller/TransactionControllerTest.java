package com.revolut.moneytransfer.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.moneytransfer.datatransferobject.TransactionDTO;
import com.revolut.moneytransfer.datatransferobject.TransactionResponseDTO;
import com.revolut.moneytransfer.domainobject.AccountDO;
import com.revolut.moneytransfer.domainobject.TransactionDO;
import com.revolut.moneytransfer.exception.ConstraintsViolationException;
import com.revolut.moneytransfer.exception.EntityNotFoundException;
import com.revolut.moneytransfer.exception.InvalidTransactionException;
import com.revolut.moneytransfer.service.transaction.TransactionService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigInteger;
import java.time.Instant;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@RunWith(SpringRunner.class)
@WebMvcTest(TransactionController.class)
public class TransactionControllerTest {

    private static Instant INSTANT_NOW = Instant.now();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TransactionService transactionService;

    private JacksonTester<TransactionDTO> transactionJson;

    private TransactionDO transactionDO;

    private AccountDO accountDO1;

    private AccountDO accountDO2;

    private TransactionDTO transactionDTO;

    private TransactionResponseDTO transactionResponseDTO;

    private ObjectMapper objectMapper;

    @Before
    public void setup() {
        objectMapper = new ObjectMapper();

        JacksonTester.initFields(this, objectMapper);

        accountDO1 = new AccountDO("Test account1", new BigInteger("100"));
        accountDO1.setId(1L);
        accountDO2 = new AccountDO("Test account2", new BigInteger("0"));
        accountDO2.setId(2L);
        transactionDO = new TransactionDO(accountDO1, accountDO2, new BigInteger("100"), INSTANT_NOW, true);
        transactionDO.setId(1L);

        transactionDTO = TransactionDTO.newBuilder()
                .setSenderId(1L)
                .setRecipientId(2L)
                .setAmount(new BigInteger("100"))
                .createTransactionDTO();

        transactionResponseDTO = TransactionResponseDTO.newBuilder()
                .setId(1L)
                .setSenderId(1L)
                .setRecipientId(2L)
                .setAmount(new BigInteger("100"))
                .setTimestamp(INSTANT_NOW)
                .setValid(true)
                .createTransactionResponseDTO();
    }

    @Test
    public void shouldRetrieveTransactionIfExist() throws Exception {
        given(transactionService.find(1L)).willReturn(transactionDO);

        MockHttpServletResponse response = mockMvc.perform(
                get("/transactions/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());
        assertEquals(transactionResponseDTO.toString(), response.getContentAsString());
    }

    @Test
    public void shouldNotRetrieveTransactionsIfDoNotExist() throws Exception {
        given(transactionService.find(99L)).willThrow(new EntityNotFoundException("Transaction unknown!"));

        MockHttpServletResponse response = mockMvc.perform(
                get("/transactions/99/transaction")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
        assertThat(response.getContentAsString()).isEmpty();
    }

    @Test
    public void shouldRetrieveTransactionsListByAccountSuccessfully() throws Exception {
        final TransactionDO transactionDO = new TransactionDO(accountDO1, accountDO2, new BigInteger("100"), INSTANT_NOW, true);

        List<TransactionDO> transactionsDO = Collections.singletonList(transactionDO);

        given(transactionService.findAllByAccountId(1L)).willReturn(transactionsDO);

        MockHttpServletResponse response = mockMvc.perform(
                get("/transactions/account/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());
        assertEquals("[{\"senderId\":1,\"recipientId\":2,\"amount\":100,\"timestamp\":\"" + INSTANT_NOW.toString() + "\",\"valid\":true}]", response.getContentAsString());
    }

    @Test
    public void shouldRetrieveEmptyTransactionsListIfAccountDoNotHaveTransactions() throws Exception {
        given(transactionService.findAllByAccountId(99L)).willReturn(Collections.emptyList());

        MockHttpServletResponse response = mockMvc.perform(
                get("/transactions/account/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());
        assertEquals("[]", response.getContentAsString());
    }


    @Test
    public void shouldCreateANewTransactionSuccessfully() throws Exception {
        given(transactionService.create(Mockito.any(TransactionDTO.class))).willReturn(transactionDO);

        MockHttpServletResponse response = mockMvc
                .perform(post("/transactions").contentType(MediaType.APPLICATION_JSON)
                        .content(transactionJson.write(transactionDTO).getJson()))
                .andReturn().getResponse();

        assertEquals(HttpStatus.CREATED.value(), response.getStatus());
        assertEquals(transactionResponseDTO.toString(), response.getContentAsString());
    }

    @Test
    public void shouldNotCreateANewTransactionIfReqFieldMissing() throws Exception {
        TransactionDTO transactionDTO = TransactionDTO.newBuilder()
                .setRecipientId(1L)
                .createTransactionDTO();

        MockHttpServletResponse response = mockMvc
                .perform(post("/transactions").contentType(MediaType.APPLICATION_JSON)
                        .content(transactionJson.write(transactionDTO).getJson()))
                .andReturn().getResponse();

        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
        assertThat(response.getContentAsString()).isEmpty();
    }

    @Test
    public void shouldNotCreateANewTransactionIfEntityNotFound() throws Exception {
        given(transactionService.create(Mockito.any(TransactionDTO.class))).willThrow(EntityNotFoundException.class);

        MockHttpServletResponse response = mockMvc
                .perform(post("/transactions").contentType(MediaType.APPLICATION_JSON)
                        .content(transactionJson.write(transactionDTO).getJson()))
                .andReturn().getResponse();

        assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
        assertThat(response.getContentAsString()).isEmpty();
    }

    @Test
    public void shouldNotCreateANewTransactionIfInvalid() throws Exception {
        given(transactionService.create(Mockito.any(TransactionDTO.class))).willThrow(InvalidTransactionException.class);

        MockHttpServletResponse response = mockMvc
                .perform(post("/transactions").contentType(MediaType.APPLICATION_JSON)
                        .content(transactionJson.write(transactionDTO).getJson()))
                .andReturn().getResponse();

        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
        assertThat(response.getContentAsString()).isEmpty();
    }

    @Test
    public void shouldRevertATransactionSuccessfully() throws Exception {
        final TransactionDO transactionDO = new TransactionDO(accountDO1, accountDO2, new BigInteger("100"), INSTANT_NOW, false);
        transactionDO.setId(1L);

        MockHttpServletResponse response = mockMvc
                .perform(patch("/transactions/1?isRevert=true").accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());
    }

    @Test
    public void shouldNotRevertATransactionIfRevertFalse() throws Exception {
        final TransactionDO transactionDO = new TransactionDO(accountDO1, accountDO2, new BigInteger("100"), INSTANT_NOW, false);
        transactionDO.setId(1L);

        MockHttpServletResponse response = mockMvc
                .perform(patch("/transactions/1?isRevert=false").accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        assertEquals(HttpStatus.NO_CONTENT.value(), response.getStatus());
    }

    @Test
    public void shouldNotRevertATransactionIfDoNotExist() throws Exception {
        doThrow(new EntityNotFoundException("Transaction unknown!")).when(transactionService).revert(99L);

        MockHttpServletResponse response = mockMvc
                .perform(patch("/transactions/99?isRevert=true").accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
    }

    @Test
    public void shouldNotRevertATransactionIfConstraintsViolated() throws Exception {
        doThrow(new ConstraintsViolationException("Some constraints are violated!")).when(transactionService).revert(99L);

        MockHttpServletResponse response = mockMvc
                .perform(patch("/transactions/99?isRevert=true").accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
    }

    @Test
    public void shouldNotRevertATransactionIfInvalidTransaction() throws Exception {
        doThrow(new InvalidTransactionException("Invalid transaction!")).when(transactionService).revert(99L);

        MockHttpServletResponse response = mockMvc
                .perform(patch("/transactions/99?isRevert=true").accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
    }

}