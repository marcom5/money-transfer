package com.revolut.moneytransfer.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.moneytransfer.datatransferobject.AccountDTO;
import com.revolut.moneytransfer.domainobject.AccountDO;
import com.revolut.moneytransfer.exception.EntityNotFoundException;
import com.revolut.moneytransfer.service.account.AccountService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@RunWith(SpringRunner.class)
@WebMvcTest(AccountController.class)
public class AccountControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AccountService accountService;

    private JacksonTester<AccountDTO> accountJson;

    private AccountDO accountDO;

    private AccountDTO accountDTO;

    private ObjectMapper objectMapper;

    @Before
    public void setup() {
        objectMapper = new ObjectMapper();

        JacksonTester.initFields(this, objectMapper);

        accountDO = new AccountDO("Test account", new BigInteger("100"));
        accountDO.setId(1L);

        accountDTO = AccountDTO.newBuilder()
                .setId(1L)
                .setName("Test account")
                .setBalance(new BigInteger("100"))
                .createAccountDTO();
    }

    @Test
    public void shouldRetrieveAccountIfExist() throws Exception {
        given(accountService.find(1L)).willReturn(accountDO);

        MockHttpServletResponse response = mockMvc.perform(
                get("/accounts/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());
        assertEquals(accountJson.write(accountDTO).getJson(), response.getContentAsString());
    }

    @Test
    public void shouldNotRetrieveAccountIfDoNotExist() throws Exception {
        given(accountService.find(99L)).willThrow(new EntityNotFoundException("Account unknown!"));

        MockHttpServletResponse response = mockMvc.perform(
                get("/accounts/99")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
        assertThat(response.getContentAsString()).isEmpty();
    }

    @Test
    public void shouldRetrieveAccountsListSuccessfully() throws Exception {
        final AccountDO accountDO2 = new AccountDO("Test Account 2", new BigInteger("100"));
        accountDO2.setId(2L);

        final AccountDTO accountDTO2 = AccountDTO.newBuilder()
                .setId(2L)
                .setName("Test Account 2")
                .setBalance(new BigInteger("100"))
                .createAccountDTO();

        final List<AccountDO> accountsDO = Arrays.asList(accountDO, accountDO2);
        final List<AccountDTO> accountsDTO = Arrays.asList(accountDTO, accountDTO2);

        given(accountService.findAll()).willReturn(accountsDO);

        MockHttpServletResponse response = mockMvc.perform(
                get("/accounts")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());
        assertEquals(objectMapper.writeValueAsString(accountsDTO), response.getContentAsString());
    }

    @Test
    public void shouldRetrieveEmptyAccountsListIfNonExists() throws Exception {
        given(accountService.findAll()).willReturn(Collections.emptyList());

        MockHttpServletResponse response = mockMvc.perform(
                get("/accounts")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());
        assertEquals("[]", response.getContentAsString());
    }

    @Test
    public void shouldCreateANewAccountSuccessfully() throws Exception {
        given(accountService.create(Mockito.any(AccountDO.class))).willReturn(accountDO);

        MockHttpServletResponse response = mockMvc
                .perform(post("/accounts").contentType(MediaType.APPLICATION_JSON)
                        .content(accountJson.write(accountDTO).getJson()))
                .andReturn().getResponse();

        assertEquals(HttpStatus.CREATED.value(), response.getStatus());
        assertEquals(accountJson.write(accountDTO).getJson(), response.getContentAsString());
    }

    @Test
    public void shouldNotCreateANewAccountIfReqFieldMissing() throws Exception {
        AccountDTO accountDTO = AccountDTO.newBuilder()
                .setId(1L)
                .createAccountDTO();

        MockHttpServletResponse response = mockMvc
                .perform(post("/accounts").contentType(MediaType.APPLICATION_JSON)
                        .content(accountJson.write(accountDTO).getJson()))
                .andReturn().getResponse();

        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
        assertThat(response.getContentAsString()).isEmpty();
    }

}